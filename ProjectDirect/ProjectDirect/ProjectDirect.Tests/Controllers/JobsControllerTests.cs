﻿using System;
using System.Collections.Generic;
using System.Web.Http.Results;
using DataAccess;
using NSubstitute;
using ProjectDirect.Controllers;
using ProjectDirect.Models;
using ProjectDirect.XmlFileCreator;
using Xunit;

namespace ProjectDirect.Tests.Controllers
{
	public class JobsControllerTests
	{
		private IRequestRepository _mockRepository;
		private IXmlRequestModelFileCreator _mockCreator;
		private JobsController _controller;

		public JobsControllerTests()
		{
			_mockRepository = Substitute.For<IRequestRepository>();
			_mockCreator = Substitute.For<IXmlRequestModelFileCreator>();
			_controller = new JobsController(_mockRepository,_mockCreator);
		}

		[Fact]
		public void SaveFiles_WhenEmptyTable_ShouldReturnNotFound()
		{
			_mockRepository.GetAll().Returns(new List<RequestEntity>());

			var result = _controller.SaveFiles();

			Assert.Equal(typeof(NotFoundResult),result.GetType());
		}

		[Fact]
		public void SaveFiles_WhenNotEmptyTable_ShouldReturnOk()
		{
			var entityModel = new RequestEntity
			{
				Index = 1, 
				Name = "Test",
				Visits = 1, 
				Date = DateTime.Now
			};

			var list = new List<RequestEntity>
			{
				entityModel
			};

			_mockRepository.GetAll().Returns(list);
			
			var result = _controller.SaveFiles();

			Assert.Equal(typeof(OkResult),result.GetType());
		}
	}
}
