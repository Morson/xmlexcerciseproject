﻿using System;
using System.Web.Http.Results;
using DataAccess;
using NSubstitute;
using ProjectDirect.Controllers;
using ProjectDirect.Models;
using Xunit;

namespace ProjectDirect.Tests.Controllers
{
	public class DataControllerTests
	{
		private IRequestRepository _mockRepository;
		private DataController _controller;

		public DataControllerTests()
		{
			_mockRepository = Substitute.For<IRequestRepository>();
			_controller = new DataController(_mockRepository);
		}

		[Fact]
		public void Post_WhenReceivedEmptyTable_ShouldReturnNotFound()
		{
			var models = new RequestModel[0];

			var result = _controller.Post(models);

			Assert.Equal(typeof(NotFoundResult),result.GetType());
		}

		[Fact]
		public void Post_WhenReceivedNotEmptyTable_ShouldReturnOk()
		{
			var models = new RequestModel[1];
			models[0] = new RequestModel {Index = 1, Name = "Test", Visits = 1, Date = DateTime.Now};

			var result = _controller.Post(models);

			Assert.Equal(typeof(OkResult),result.GetType());
		}
	}
}
