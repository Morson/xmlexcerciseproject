﻿using System.Collections.Generic;

namespace DataAccess
{
	public interface IRequestRepository
	{
		IList<RequestEntity> GetAll();
		void AddMany(IEnumerable<RequestEntity> requestModels);
	}
}