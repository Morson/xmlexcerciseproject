﻿using Autofac;

namespace DataAccess
{
	public class DataAccessModule:Autofac.Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<DbFactory>()  
				.As<IDbFactory>()  
				.InstancePerLifetimeScope();  
  
			builder.RegisterType<RequestRepository>()  
				.As<IRequestRepository>()  
				.InstancePerLifetimeScope();
		}
	}
}
