﻿namespace DataAccess
{
	public interface IDbFactory
	{
		RequestsDbContext Init();
	}
}
