﻿using System.Collections.Generic;
using System.Linq;

namespace DataAccess
{
	public class RequestRepository : IRequestRepository
	{
	    private RequestsDbContext _dbContext;

		protected IDbFactory DbFactory { get; private set; }

		protected RequestsDbContext DbContext
		{
			get { return _dbContext ?? (_dbContext = DbFactory.Init()); }
		}

	    public RequestRepository(IDbFactory dbFactory)
	    {
		    DbFactory = dbFactory;
	    }

	    public IList<RequestEntity> GetAll()
	    {
		   return DbContext.Requests.ToList();
	    }

	    public void AddMany(IEnumerable<RequestEntity> requestModels)
	    {
		    foreach (var request in requestModels)
		    {
			    DbContext.Requests.Add(request);
		    }

		    DbContext.SaveChanges();
	    }
    }
}
