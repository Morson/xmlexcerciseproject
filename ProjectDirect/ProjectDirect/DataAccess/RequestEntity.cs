﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DataAccess
{
	public class RequestEntity
	{
		[Key]
		public int Id { get;set; }
		public int Index { get; set; }
		public string Name { get; set; }
		public int? Visits { get; set; }
		public DateTime Date { get; set; }
	}
}