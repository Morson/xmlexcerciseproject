﻿using System.Data.Entity;

namespace DataAccess
{
	public class RequestsDbContext : DbContext
	{
		public RequestsDbContext()
			: base("name=RequestDbContext")
		{}

		public DbSet<RequestEntity> Requests { get; set; }
	}
}