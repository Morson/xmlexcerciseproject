﻿namespace DataAccess
{
	public class DbFactory : IDbFactory
	{
		RequestsDbContext _dbContext;

		public RequestsDbContext Init()
		{
			return _dbContext ?? (_dbContext = new RequestsDbContext());
		}
	}
}