﻿using System;
using System.Configuration;
using System.Linq;
using Bogus;
using Bogus.Extensions;
using RestSharp;

namespace ConsoleProjectDirect
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var count = Convert.ToInt32(args.Single());
			var data = GenerateRequestModel(count);

			Send(data);
		}
		
		private static void Send(RequestModel[] models)
		{
			var address = ConfigurationManager.AppSettings.Get("APIAddress");
			var client = new RestClient(address);
			var request = new RestRequest("api/data", Method.POST);
			request.AddJsonBody(models);
			client.Execute(request);
		}

		private static RequestModel[] GenerateRequestModel(int requestNumber)
		{
			var requestModelFaker = new Faker<RequestModel>()
				.RuleFor(rm => rm.Index, f => f.IndexFaker)
				.RuleFor(rm => rm.Name, f => f.Lorem.Word())
				.RuleFor(rm => rm.Visits, f => f.Random.Number(int.MaxValue - 1).OrNull(f))
				.RuleFor(rm => rm.Date, f => f.Date.Past());

			return requestModelFaker.Generate(requestNumber).ToArray();
		}
	}
}
