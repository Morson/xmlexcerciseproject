﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using DataAccess;
using ProjectDirect.Models;

namespace ProjectDirect.XmlFileCreator
{
	public class XmlRequestModelFileCreator : IXmlRequestModelFileCreator
	{
		private readonly string _appDataDirectory;
		private readonly XmlSerializer _serializer;
		private readonly XmlSerializerNamespaces _namespaces;
		public XmlRequestModelFileCreator()
		{
			_appDataDirectory= AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
			_serializer =  new XmlSerializer(typeof(XmlRequestModel));
			_namespaces = new XmlSerializerNamespaces(
				new[] {XmlQualifiedName.Empty});
		}

		public void CreateXmlRequestModelFile(RequestEntity model)
		{
			var fileDirectory = CreateDirectoryPath(model.Date);
			var filePath = Path.Combine(fileDirectory, $"{model.Index}.xml");

			using (var writer = new StreamWriter(filePath))
			{
				var xmlRequestModel = model.MapToXmlRequestModel();

				_serializer.Serialize(writer, xmlRequestModel, _namespaces);
			}
		}

		private string CreateDirectoryPath(DateTime date)
		{
			var formattedDate = date.ToString("yyyy-MM-dd");
			var fileDirectory = Path.Combine(_appDataDirectory, "xml", formattedDate);

			if (!Directory.Exists(fileDirectory))
			{
				Directory.CreateDirectory(fileDirectory);
			}

			return fileDirectory;
		}
	}
}