﻿using DataAccess;

namespace ProjectDirect.XmlFileCreator
{
	public interface IXmlRequestModelFileCreator
	{
		void CreateXmlRequestModelFile(RequestEntity model);
	}
}