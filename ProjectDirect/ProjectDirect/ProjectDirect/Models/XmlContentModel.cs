﻿using System;
using System.Xml.Serialization;

namespace ProjectDirect.Models
{
	public class XmlContentModel
	{
		public XmlContentModel()
		{
			
		}
		public XmlContentModel(string name,DateTime dateRequested,int? visits)
		{
			Name = name;
			DateRequested = dateRequested.ToString("yyyy-MM-dd");
			Visits = visits;
		}
		[XmlElement("name")]
		public string Name { get; set; }

		[XmlElement("visits")]
		public int? Visits { get; set; }
			
		[XmlIgnore]
		public bool VisitsSpecified => this.Visits != null;

		[XmlElement("dateRequested")]
		public string DateRequested { get; set; }
	}
}