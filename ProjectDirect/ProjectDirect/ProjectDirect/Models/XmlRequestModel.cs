﻿using System;
using System.Xml.Serialization;

namespace ProjectDirect.Models
{
	[XmlRoot("request")]
	public class XmlRequestModel
	{
		public XmlRequestModel()
		{
			
		}
		public XmlRequestModel(int index,string name,DateTime dateRequested,int? visits)
		{
			Index = index;
			XmlContentModel = new XmlContentModel(name, dateRequested, visits);
		}

		[XmlElement("ix")]
		public int Index { get; set; }
		
		[XmlElement("content")]
		public XmlContentModel XmlContentModel { get; set; }
	}
}