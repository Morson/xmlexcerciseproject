﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess;

namespace ProjectDirect.Models
{
	public static class Extensions
	{
		public static RequestEntity MapToEntity(this RequestModel requestModel)
		{
			return new RequestEntity
			{
				Index = requestModel.Index,
				Date = requestModel.Date,
				Name = requestModel.Name,
				Visits = requestModel.Visits
			};
		}

		public static XmlRequestModel MapToXmlRequestModel(this RequestEntity requestEntity)
		{
			return 
				new XmlRequestModel(requestEntity.Index,requestEntity.Name,requestEntity.Date,requestEntity.Visits);
		}
	}
}