﻿using System.Web.Http;
using Migrations;

namespace ProjectDirect.App_Start
{
	public class Bootstrapper
	{
		public static void Run()  
		{  
			AutofacWebapiConfig.Initialize(GlobalConfiguration.Configuration);
			Migrator.Migrate();
		}  
	}
}