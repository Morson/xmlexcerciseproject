using System.Web.Http;
using WebActivatorEx;
using ProjectDirect;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace ProjectDirect
{
    public class SwaggerConfig
    {
	    public static void Register()
	    {
		    var thisAssembly = typeof(SwaggerConfig).Assembly;

		    GlobalConfiguration.Configuration
			    .EnableSwagger(c => { c.SingleApiVersion("v1", "ProjectDirect"); })
			    .EnableSwaggerUi(c => { });
	    }
    }
}
