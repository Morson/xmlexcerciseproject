﻿using System;
using System.IO;
using System.Linq;
using System.Web.Http;
using System.Xml;
using System.Xml.Serialization;
using DataAccess;
using ProjectDirect.Models;
using ProjectDirect.XmlFileCreator;

namespace ProjectDirect.Controllers
{
	[RoutePrefix("api/jobs")]
    public class JobsController : ApiController
    {
	    private readonly IRequestRepository _requestRepository;
	    private readonly IXmlRequestModelFileCreator _creator;

	    public JobsController(IRequestRepository requestRepository,IXmlRequestModelFileCreator creator)
	    {
		    _requestRepository = requestRepository;
			_creator = creator;
	    }

	    [HttpGet, Route("saveFiles")]
	    public IHttpActionResult SaveFiles()
	    {
		    var requests = _requestRepository.GetAll();

		    if (requests == null || !requests.Any())
			    return NotFound();

		    foreach (var request in requests)
		    {
			    _creator.CreateXmlRequestModelFile(request);
		    }

		    return Ok();
	    }
    }
}
