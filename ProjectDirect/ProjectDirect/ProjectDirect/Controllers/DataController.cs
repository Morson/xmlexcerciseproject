﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using DataAccess;
using ProjectDirect.Models;

namespace ProjectDirect.Controllers
{
    public class DataController : ApiController
    {
	    private readonly IRequestRepository _requestRepository;

	    public DataController(IRequestRepository requestRepository)
	    {
		    _requestRepository = requestRepository;
	    }

	    public IHttpActionResult Post([FromBody]RequestModel[] models)
	    {
		    if (models == null || models.Length == 0)
		    {
			    return NotFound();
		    }

		    var list = new List<RequestEntity>();
		    
		    foreach (var model in models)
		    {
			    list.Add(model.MapToEntity());
		    }

		    _requestRepository.AddMany(list);

		    return Ok();
	    }
    }
}
