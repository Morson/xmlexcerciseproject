﻿CREATE TABLE [Requests] (
	[Id] int IDENTITY(1,1) NOT NULL,
    [Index] int,
    [Name] varchar(255),
    [Visits] int,
    [Date] datetime

	CONSTRAINT [PK_Requests] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)
);