﻿using Autofac;

namespace Migrations
{
	public class MigrationsModule:Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder
				.RegisterType<Migrator>()
				.AsSelf()
				.SingleInstance();
		}
	}
}
