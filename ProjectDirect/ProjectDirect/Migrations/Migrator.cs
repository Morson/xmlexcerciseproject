﻿using System.Configuration;
using System.Diagnostics;
using System.Reflection;
using DbUp;

namespace Migrations
{
	public class Migrator
	{
		public static void Migrate()
		{
			var connectionString = ConfigurationManager.ConnectionStrings["RequestDbContext"].ConnectionString;

			EnsureDatabase.For.SqlDatabase(connectionString);

			var upgrader =
				DeployChanges.To
					.SqlDatabase(connectionString)
					.WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
					.LogToConsole()
					.Build();

			var result = upgrader.PerformUpgrade();

			if (!result.Successful)
			{
				Debug.WriteLine(result.Error);
			}
			
			Debug.WriteLine("Success!");
		}
	}
}
